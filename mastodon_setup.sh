#!/bin/sh
#
# bootstrap things like db schemas before 'docker-compose up' can be fully run

set -eux

docker buildx build -t mastodon_selfbuilt -f ./Dockerfile.mastodon .
docker-compose run --rm mastodon_web rails db:migrate

