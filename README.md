# pubtester

a set of scripts to spin up local environments containing many fediverse implementations for federation testing

at the moment only two instances of pleroma are generated.

mastodon is TODO.

## setup

add the following to your `/etc/hosts`
```
127.0.0.1		pleroma.pubtester.example.net		localhost
127.0.0.1		pleroma2.pubtester.example.net	localhost
```

run
```
git clone ... && cd pubtester
./pleroma_setup.sh
docker-compose up -d pleroma_web pleroma2_web nginx
```

then go to `https://pleroma.pubtester.example.net:20000/main/friends`
and also `https://pleroma2.pubtester.example.net:20000/main/friends`

## emit new minica certs

this is generally for myself: https://github.com/jsha/minica

`minica -domains 'pubtester.example.net,*.pubtester.example.net'`


## wipe everything

```
sudo rm -rfv ./data
./pleroma_setup.sh
```
