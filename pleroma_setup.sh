#!/bin/sh
#
# setup pleroma db

set -eux
docker-compose up -d pleroma_db
docker-compose up -d pleroma2_db
sleep 10 # waiting for databases to warm up
docker-compose exec -i pleroma_db psql -U pleroma -c "CREATE EXTENSION IF NOT EXISTS citext;"
docker-compose exec -i pleroma_db psql -U pleroma -c "CREATE EXTENSION IF NOT EXISTS pg_trgm;"
docker-compose exec -i pleroma_db psql -U pleroma -c 'CREATE EXTENSION IF NOT EXISTS "uuid-ossp";'
docker-compose exec -i pleroma2_db psql -U pleroma -c "CREATE EXTENSION IF NOT EXISTS citext;"
docker-compose exec -i pleroma2_db psql -U pleroma -c "CREATE EXTENSION IF NOT EXISTS pg_trgm;"
docker-compose exec -i pleroma2_db psql -U pleroma -c 'CREATE EXTENSION IF NOT EXISTS "uuid-ossp";'
docker-compose down

docker buildx build -t pleroma_selfbuilt -f ./Dockerfile.pleroma .
docker buildx build -t pleroma_source_selfbuilt -f ./Dockerfile.pleroma-source .
docker buildx build -t pubtester_gotosocial -f ./Dockerfile.gotosocial .
